package com.example.farmermarket.data;

import android.content.Context;

import com.example.farmermarket.prefs.AppPrefsHelper;

import javax.inject.Inject;

public class AppDataManager implements DataHelper {
    private final AppPrefsHelper mPreferencesHelper;

    private final Context mContext;

    @Inject
    public AppDataManager(Context context, AppPrefsHelper preferencesHelper) {
        mContext = context;
        mPreferencesHelper = preferencesHelper;
    }
}
