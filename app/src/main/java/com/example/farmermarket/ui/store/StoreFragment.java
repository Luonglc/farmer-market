package com.example.farmermarket.ui.store;

import android.os.Bundle;
import android.view.View;

import com.example.farmermarket.BR;
import com.example.farmermarket.R;
import com.example.farmermarket.adapter.SlideAdapter;
import com.example.farmermarket.base.BaseFragment;
import com.example.farmermarket.databinding.FragmentStoreBinding;
import com.example.farmermarket.di.factory.ViewModelFactory;
import com.example.farmermarket.ui.store.freshtoday.FreshToDayAdapter;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class StoreFragment extends BaseFragment<FragmentStoreBinding, StoreViewModel> implements StoreNavigator {

    private FragmentStoreBinding binding;
    private StoreViewModel viewModel;
    private static StoreFragment INSTANCE;

    @Inject SlideAdapter slideAdapter;
    @Inject
    FreshToDayAdapter toDayAdapter;

    @Inject
    ViewModelFactory factory;

    public static StoreFragment getInstance(){
        if(INSTANCE == null){
            INSTANCE = new StoreFragment();
        }
        return INSTANCE;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_store;
    }

    @Override
    public StoreViewModel getViewModel() {
        viewModel = ViewModelProviders.of(this, factory).get(StoreViewModel.class);
        return viewModel;
    }


    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = getViewDataBinding();
        viewModel.setNavigator(this);
        obserableData();

        binding.slider.setSliderAdapter(slideAdapter);
        binding.slider.setScrollTimeInSec(5000);
        binding.slider.startAutoCycle();
        binding.slider.setIndicatorAnimation(IndicatorAnimations.WORM);
        binding.slider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(RecyclerView.HORIZONTAL);
        binding.rcvFreshToday.setLayoutManager(layoutManager);
        binding.rcvFreshToday.setAdapter(toDayAdapter);
        binding.rcvFreshToday.setNestedScrollingEnabled(false);
    }

    private void obserableData(){
        viewModel.getItemLive().observe(getBaseActivity(),items->{
            slideAdapter.renewItems(items);
        });

        viewModel.getItemFresh().observe(getBaseActivity(),items->{
            toDayAdapter.addItems(items);
        });
    }
}
