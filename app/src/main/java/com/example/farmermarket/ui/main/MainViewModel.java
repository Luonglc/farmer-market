package com.example.farmermarket.ui.main;

import com.example.farmermarket.base.BaseViewModel;
import com.example.farmermarket.data.AppDataManager;
import com.example.farmermarket.rx.SchedulerProvider;
import com.example.farmermarket.ui.main.MainNavigator;

public class MainViewModel extends BaseViewModel<MainNavigator> {

    public MainViewModel(AppDataManager appDataManager, SchedulerProvider schedulerProvider) {
        super(appDataManager, schedulerProvider);
    }
}
