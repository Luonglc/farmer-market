package com.example.farmermarket.ui.user;

import com.example.farmermarket.base.BaseViewModel;
import com.example.farmermarket.data.AppDataManager;
import com.example.farmermarket.rx.SchedulerProvider;

public class UserViewModel extends BaseViewModel<UserNavigator> {

    public UserViewModel(AppDataManager appDataManager, SchedulerProvider schedulerProvider) {
        super(appDataManager, schedulerProvider);
    }
}
