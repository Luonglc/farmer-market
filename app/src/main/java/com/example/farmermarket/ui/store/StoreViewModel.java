package com.example.farmermarket.ui.store;

import com.example.farmermarket.base.BaseViewModel;
import com.example.farmermarket.data.AppDataManager;
import com.example.farmermarket.model.Item;
import com.example.farmermarket.rx.SchedulerProvider;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class StoreViewModel extends BaseViewModel<StoreNavigator> {
    private final MutableLiveData<List<Item>> itemLive;
    private final MutableLiveData<List<Item>> itemFresh;

    public StoreViewModel(AppDataManager appDataManager, SchedulerProvider schedulerProvider) {
        super(appDataManager, schedulerProvider);
        itemLive = new MutableLiveData<>();
        itemLive.postValue(new ArrayList<>());
        itemFresh = new MutableLiveData<>();
        itemFresh.postValue(new ArrayList<>());
        dummyData();
    }

    public LiveData<List<Item>> getItemLive() {
        return itemLive;
    }

    public LiveData<List<Item>> getItemFresh() {
        return itemFresh;
    }

    public void dummyData(){
        List<Item> items = new ArrayList<>();
        for(int i = 0 ; i < 5 ; i++){
            Item item = new Item();
            item.setName("Chanh tuoi"+i);
            item.setImageUrl("https://lh3.googleusercontent.com/proxy/jvMsbuPq_z6ZMPR2eiNgAQeifDEhEZYyjdbW288rLl9Dm1A0pva11TpwjnPdE6LijnxQ6ibdb2pQKcOEADMaReoSozCxbiZP4tN9kvVRtN4d8P3BZyxJJpVJ6dVyl5T2Dtpy61TmlW_k-K0pPSXzmNAisGUK");
            item.setAddr("Thanh Hoa");
            item.setPrice("150.000 đ/kg");
            item.setTimeStamp("1 giờ trước");
            item.setDescription("Chanh vườn thơm ngon đảm bảo chất lượng thơm hơn tất cả các loại chanh vùng khác.");
            items.add(item);
        }
        itemLive.postValue(items);
        itemFresh.postValue(items);
    }
}
