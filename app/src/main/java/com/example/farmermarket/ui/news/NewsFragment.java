package com.example.farmermarket.ui.news;

import android.os.Bundle;

import com.example.farmermarket.BR;
import com.example.farmermarket.R;
import com.example.farmermarket.base.BaseFragment;
import com.example.farmermarket.databinding.FragmentNewBinding;
import com.example.farmermarket.di.factory.ViewModelFactory;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;

public class NewsFragment extends BaseFragment<FragmentNewBinding,NewsViewModel> implements NewsNavigator {
    private FragmentNewBinding binding;
    private NewsViewModel viewModel;
    private static NewsFragment INSTANCE;
    @Inject
    ViewModelFactory factory;

    public static NewsFragment getInstance(){
        if(INSTANCE == null){
            INSTANCE = new NewsFragment();
        }
        return INSTANCE;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_new;
    }

    @Override
    public NewsViewModel getViewModel() {
        viewModel = ViewModelProviders.of(this,factory).get(NewsViewModel.class);
        return viewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = getViewDataBinding();
        viewModel.setNavigator(this);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser){

        }
    }
}
