package com.example.farmermarket.ui.store.freshtoday;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.farmermarket.R;
import com.example.farmermarket.base.BaseViewHolder;
import com.example.farmermarket.base.BaseViewModel;
import com.example.farmermarket.data.AppDataManager;
import com.example.farmermarket.databinding.ItemTodayBinding;
import com.example.farmermarket.model.Item;
import com.example.farmermarket.rx.SchedulerProvider;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class FreshToDayAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    private List<Item> list;
    @Inject
    Context context;
    @Inject
    public FreshToDayAdapter(Context context) {
        this.context = context;
        list = new ArrayList<>();
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemTodayBinding binding =ItemTodayBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false);
        return new MViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void reAddItems(List<Item> items){
        list.clear();
        list.addAll(items);
        notifyDataSetChanged();
    }

    public void addItems(List<Item> items){
        list.addAll(items);
        notifyDataSetChanged();
    }

    class MViewHolder extends BaseViewHolder implements ItemViewModel.OnClickListener {
        private ItemTodayBinding binding;
        private ItemViewModel viewModel;

        public MViewHolder(ItemTodayBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        @Override
        public void onBind(int position) {
            final Item item = list.get(position);
            viewModel = new ItemViewModel(item, this);
            binding.setViewModel(viewModel);
            int color = context.getResources().getColor(R.color.purple_light);
            switch (position%4){
                case 0:
                    color =  context.getResources().getColor(R.color.purple_light);
                    break;
                case 1:
                    color =  context.getResources().getColor(R.color.blue_light);
                    break;
                case 2:
                    color =  context.getResources().getColor(R.color.green_light);
                    break;
                case 3:
                    color =  context.getResources().getColor(R.color.orange_light);
                    break;
            }
            binding.cardItemView.setCardBackgroundColor(color);
            binding.executePendingBindings();
        }

        @Override
        public void onItemClick() {

        }
    }
}
