package com.example.farmermarket.ui.splash;

import com.example.farmermarket.base.BaseViewModel;
import com.example.farmermarket.data.AppDataManager;
import com.example.farmermarket.rx.SchedulerProvider;

public class SplashViewModel extends BaseViewModel<SplashNavigator> {

    public SplashViewModel(AppDataManager appDataManager, SchedulerProvider schedulerProvider) {
        super(appDataManager, schedulerProvider);
    }
}
