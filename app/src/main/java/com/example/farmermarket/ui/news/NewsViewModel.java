package com.example.farmermarket.ui.news;

import com.example.farmermarket.base.BaseViewModel;
import com.example.farmermarket.data.AppDataManager;
import com.example.farmermarket.rx.SchedulerProvider;

public class NewsViewModel extends BaseViewModel<NewsNavigator> {

    public NewsViewModel(AppDataManager appDataManager, SchedulerProvider schedulerProvider) {
        super(appDataManager, schedulerProvider);
    }
}
