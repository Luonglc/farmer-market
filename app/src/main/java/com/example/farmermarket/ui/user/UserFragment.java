package com.example.farmermarket.ui.user;

import android.os.Bundle;

import com.example.farmermarket.BR;
import com.example.farmermarket.R;
import com.example.farmermarket.base.BaseFragment;
import com.example.farmermarket.databinding.FragmentUserBinding;
import com.example.farmermarket.di.factory.ViewModelFactory;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

public class UserFragment extends BaseFragment<FragmentUserBinding,UserViewModel> implements UserNavigator {
    private FragmentUserBinding binding;
    private UserViewModel viewModel;
    private static UserFragment INSTANCE;
    @Inject
    ViewModelFactory factory;

    public static UserFragment getInstance(){
        if(INSTANCE == null){
            INSTANCE = new UserFragment();
        }
        return INSTANCE;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_user;
    }

    @Override
    public UserViewModel getViewModel() {
        viewModel = ViewModelProviders.of(this,factory).get(UserViewModel.class);
        return viewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = getViewDataBinding();
        viewModel.setNavigator(this);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser){

        }
    }
}
