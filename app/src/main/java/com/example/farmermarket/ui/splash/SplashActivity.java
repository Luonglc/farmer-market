package com.example.farmermarket.ui.splash;

import androidx.annotation.RequiresApi;
import androidx.databinding.library.baseAdapters.BR;
import androidx.lifecycle.ViewModelProviders;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import com.example.farmermarket.ui.main.MainActivity;
import com.example.farmermarket.R;
import com.example.farmermarket.base.BaseActivity;
import com.example.farmermarket.databinding.ActivitySplashBinding;
import com.example.farmermarket.di.factory.ViewModelFactory;

import javax.inject.Inject;

public class SplashActivity extends BaseActivity<ActivitySplashBinding,SplashViewModel> implements SplashNavigator {

    private ActivitySplashBinding binding;
    private SplashViewModel viewModel;

    @Inject
    ViewModelFactory factory;
    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    public SplashViewModel getViewModel() {
        viewModel = ViewModelProviders.of(this,factory).get(SplashViewModel.class);
        return viewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = getViewDataBinding();
        viewModel.setNavigator(this);

        new Handler().postDelayed(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void run() {
//                Pair[] pairs = new Pair[1];
//                pairs[0] = new Pair<View,String>(binding.ivTitle,getResources().getString(R.string.title_app_trandition));
//                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(SplashActivity.this,pairs);
//                startActivity(new Intent(SplashActivity.this,MainActivity.class),options.toBundle());
//                finish();
                showActivity(MainActivity.class);
                finish();
            }
        }, 2000);
    }
}
