package com.example.farmermarket.ui.main;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProviders;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

import android.os.Bundle;
import android.view.MenuItem;

import com.example.farmermarket.BR;
import com.example.farmermarket.R;
import com.example.farmermarket.base.BaseActivity;
import com.example.farmermarket.databinding.ActivityMainBinding;
import com.example.farmermarket.di.factory.ViewModelFactory;
import com.example.farmermarket.ui.news.NewsFragment;
import com.example.farmermarket.ui.store.StoreFragment;
import com.example.farmermarket.ui.user.UserFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class MainActivity extends BaseActivity<ActivityMainBinding, MainViewModel> implements MainNavigator, HasSupportFragmentInjector {

    private ActivityMainBinding binding;
    private MainViewModel viewModel;

    @Inject
    ViewModelFactory factory;
    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;
    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public MainViewModel getViewModel() {
        viewModel = ViewModelProviders.of(this,factory).get(MainViewModel.class);
        return viewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = getViewDataBinding();
        viewModel.setNavigator(this);

        FragmentManager manager =getSupportFragmentManager();
        StoreFragment storeFragment = StoreFragment.getInstance();
        NewsFragment newsFragment = NewsFragment.getInstance();
        UserFragment userFragment = UserFragment.getInstance();

        List<Fragment> listFr = new ArrayList<>();
        listFr.add(storeFragment);
        listFr.add(newsFragment);
        listFr.add(userFragment);
        ViewPagerAdapter pagerAdapter = new ViewPagerAdapter(manager, listFr);
        binding.viewpager.setAdapter(pagerAdapter);
        binding.viewpager.setOffscreenPageLimit(listFr.size());
        binding.bottomNavigation.setItemIconTintList(null);
        binding.bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.tab_store:
                        binding.viewpager.setCurrentItem(0, false);
                        break;
                    case R.id.tab_news:
                        binding.viewpager.setCurrentItem(1, false);
                        break;
                    case R.id.tab_user:
                        binding.viewpager.setCurrentItem(2, false);
                        break;
                }
                return true;
            }
        });
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }
}
