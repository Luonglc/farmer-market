package com.example.farmermarket.ui.store.freshtoday;

import com.example.farmermarket.model.Item;

import androidx.databinding.ObservableField;

public class ItemViewModel {
    public final ObservableField<String> name;
    public final ObservableField<String> imageUrl;
    public final ObservableField<String> timeStamp;
    public final ObservableField<String> addr;
    public final ObservableField<String> price;

    public final Item item;
    public final OnClickListener listener;

    public ItemViewModel(Item item, OnClickListener listener) {
        this.item = item;
        this.listener = listener;
        name = new ObservableField<>(item.getName());
        imageUrl = new ObservableField<>(item.getImageUrl());
        timeStamp = new ObservableField<>(item.getTimeStamp());
        addr = new ObservableField<>(item.getAddr());
        price = new ObservableField<>(item.getPrice());
    }

    public interface OnClickListener{
        void onItemClick();
    }
}
