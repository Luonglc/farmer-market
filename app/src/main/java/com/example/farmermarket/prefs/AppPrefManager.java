package com.example.farmermarket.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;

public class AppPrefManager implements AppPrefsHelper {
    private static final String PREF_FILE_NAME = "PREF_FILE_NAME";
    private final SharedPreferences mPrefs;

    @Inject
    public AppPrefManager(Context context){
        mPrefs =context.getSharedPreferences(PREF_FILE_NAME,Context.MODE_PRIVATE);
    }
}
