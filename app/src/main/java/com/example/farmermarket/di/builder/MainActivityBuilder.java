package com.example.farmermarket.di.builder;

import com.example.farmermarket.ui.news.NewsFragment;
import com.example.farmermarket.ui.store.StoreFragment;
import com.example.farmermarket.ui.user.UserFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainActivityBuilder {
    @ContributesAndroidInjector
    abstract StoreFragment bindStoreFragment();

    @ContributesAndroidInjector
    abstract NewsFragment bindNewsFragment();

    @ContributesAndroidInjector
    abstract UserFragment bindUserFragment();
}
