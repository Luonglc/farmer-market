package com.example.farmermarket.di.component;

import android.app.Application;

import com.example.farmermarket.FarmerApplication;
import com.example.farmermarket.di.builder.ActivityBuilder;
import com.example.farmermarket.di.module.AppModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Singleton
@Component(modules = {AndroidInjectionModule.class, AppModule.class, ActivityBuilder.class})
public interface AppComponent {
    void inject(FarmerApplication app);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder appliation(Application application);

        AppComponent build();
    }
}
