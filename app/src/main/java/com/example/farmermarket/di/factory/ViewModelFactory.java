package com.example.farmermarket.di.factory;

import com.example.farmermarket.data.AppDataManager;
import com.example.farmermarket.rx.SchedulerProvider;
import com.example.farmermarket.ui.main.MainViewModel;
import com.example.farmermarket.ui.news.NewsViewModel;
import com.example.farmermarket.ui.splash.SplashViewModel;
import com.example.farmermarket.ui.store.StoreFragment;
import com.example.farmermarket.ui.store.StoreViewModel;
import com.example.farmermarket.ui.user.UserViewModel;

import javax.inject.Inject;
import javax.inject.Singleton;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

@Singleton
public class ViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private final AppDataManager appDataManager;
    private final SchedulerProvider schedulerProvider;
    @Inject
    public ViewModelFactory(AppDataManager appDataManager,
                            SchedulerProvider schedulerProvider) {
        this.appDataManager = appDataManager;
        this.schedulerProvider = schedulerProvider;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(MainViewModel.class)) {
            return (T) new MainViewModel(appDataManager, schedulerProvider);
        }
        if (modelClass.isAssignableFrom(SplashViewModel.class)) {
            return (T) new SplashViewModel(appDataManager, schedulerProvider);
        }
        if (modelClass.isAssignableFrom(StoreViewModel.class)) {
            return (T) new StoreViewModel(appDataManager, schedulerProvider);
        }
        if (modelClass.isAssignableFrom(NewsViewModel.class)) {
            return (T) new NewsViewModel(appDataManager, schedulerProvider);
        }
        if (modelClass.isAssignableFrom(UserViewModel.class)) {
            return (T) new UserViewModel(appDataManager, schedulerProvider);
        }
        throw new IllegalArgumentException("Unknown ViewModel class: " + modelClass.getName());
    }
}
