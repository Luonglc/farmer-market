package com.example.farmermarket.di.module;

import android.app.Application;
import android.content.Context;

import com.example.farmermarket.data.AppDataManager;
import com.example.farmermarket.data.DataHelper;
import com.example.farmermarket.prefs.AppPrefManager;
import com.example.farmermarket.prefs.AppPrefsHelper;
import com.example.farmermarket.rx.AppSchedulerProvider;
import com.example.farmermarket.rx.SchedulerProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    @Singleton
    @Provides
    Context provideContext(Application application) {
        return application;
    }

    @Singleton
    @Provides
    AppPrefsHelper providePrefHelper(AppPrefManager prefManager) {
        return prefManager;
    }

    @Singleton
    @Provides
    DataHelper provideAppDataManager(AppDataManager appDataManager){
        return appDataManager;
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }
}
