package com.example.farmermarket.base;

import com.example.farmermarket.data.AppDataManager;
import com.example.farmermarket.rx.SchedulerProvider;

import java.lang.ref.WeakReference;

import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.disposables.CompositeDisposable;

public abstract class BaseViewModel<N> extends ViewModel {
    private final AppDataManager mAppDataManager;
    private final ObservableBoolean mIsLoading = new ObservableBoolean();
    public final MutableLiveData<Boolean> mshowloadding = new MutableLiveData<>();
    private CompositeDisposable compositeDisposable;
    private final SchedulerProvider mSchedulerProvider;

    private WeakReference<N> navigator;

    public BaseViewModel(AppDataManager appDataManager,  SchedulerProvider schedulerProvider) {
        this.mAppDataManager = appDataManager;
        this.mSchedulerProvider = schedulerProvider;
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    protected void onCleared() {
        compositeDisposable.dispose();
        super.onCleared();

    }

    public void setNavigator(N mNavigator) {
        this.navigator = new WeakReference<>(mNavigator);
    }

    public AppDataManager getmAppDataManager() {
        return mAppDataManager;
    }

    public ObservableBoolean getmIsLoading() {
        return mIsLoading;
    }

    public N getNavigator() {
        return navigator.get();
    }

    public CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }

    public MutableLiveData<Boolean> getMshowloadding() {
        return mshowloadding;
    }

    public void setIsLoading(boolean isLoading) {
        mIsLoading.set(isLoading);
        mshowloadding.setValue(isLoading);
    }
}
