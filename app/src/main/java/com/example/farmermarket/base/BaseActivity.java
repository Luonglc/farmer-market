package com.example.farmermarket.base;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.androidnetworking.common.ANConstants;
import com.androidnetworking.error.ANError;
import com.example.farmermarket.R;

import org.json.JSONException;
import org.json.JSONObject;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import dagger.android.AndroidInjection;

public abstract class BaseActivity<T extends ViewDataBinding, V extends BaseViewModel> extends AppCompatActivity implements BaseFragment.Callback {
    private ProgressDialog progressDialog;
    private T viewDataBinding;
    private V viewModel;

    public abstract int getBindingVariable();

    public abstract @LayoutRes
    int getLayoutId();

    public abstract V getViewModel();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        performDependencyInjection();
        super.onCreate(savedInstanceState);
        performDataBinding();
    }

    private void performDataBinding() {
        viewDataBinding = DataBindingUtil.setContentView(this, getLayoutId());
        this.viewModel = viewModel == null ? getViewModel() : viewModel;
        viewDataBinding.setVariable(getBindingVariable(), viewModel);
        viewDataBinding.executePendingBindings();
    }

    public T getViewDataBinding() {
        return viewDataBinding;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasPermission(String permission) {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermissionsSafely(String[] permissions, int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode);
        }
    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    public void hideLoading() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
    }

    public void showLoading() {
        hideLoading();
        // show
    }

    public boolean isNetworkConnected() {
        return true;
//        return NetworkUtils.isNetworkConnected(getApplicationContext());
    }

    public void performDependencyInjection() {
        AndroidInjection.inject(this);
    }

    public void showActivity(Class t) {
        Intent intent = new Intent(this, t);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void showActivity(Class t, Bundle bundle) {
        Intent intent = new Intent(this, t);
        intent.putExtra("extra", bundle);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void showActivityWithResult(Class t, Bundle bundle, int requestCode) {
        Intent intent = new Intent(this, t);
        intent.putExtra("extra", bundle);
        startActivityForResult(intent, requestCode);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void showActivityWithFlag(Class t) {
        Intent intent = new Intent(this, t);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public Bundle getBundle() {
        return getIntent().getBundleExtra("extra");
    }

    public void finishActivity() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void setObservableLoadding(BaseViewModel viewModel) {
//        viewModel.getLoading().observe(this, new Observer<Boolean>() {
//            @Override
//            public void onChanged(Boolean loading) {
//                if (loading) {
//                    showLoadingDialog();
//                } else {
//                    hideLoadingDialog();
//                }
//            }
//        });
    }

    public void displayError(Throwable throwable) {
//        if (throwable == null) {
//            return;
//        }
//        if (throwable instanceof ANError) {
//            ANError error = (ANError) throwable;
//            if (error.getErrorDetail().equals(ANConstants.CONNECTION_ERROR)) {
//                DialogUtils.showAlert1Btn(this, "", getString(R.string.btn_ok), getString(R.string.no_network), null);
//                return;
//            }
//            try {
//                if (error.getErrorBody() != null) {
//                    JSONObject jsonObject = new JSONObject(error.getErrorBody());
//                    int errorCode = jsonObject.optInt("error_code");
//                    String errorMessage = CommonUtils.getErrorMessageByCode(this, errorCode);
//
//                    Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
//                } else {
//                    DialogUtils.showAlert1Btn(this, "", getString(R.string.btn_ok), getString(R.string.no_network), null);
//                    return;
//                }
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//        }
    }

//    private void showLoadingDialog() {
//        DialogUtils.showLoadingDialog(this);
//    }
//
//    private void hideLoadingDialog() {
//        DialogUtils.dismissAlert();
//    }

    public boolean loadFragment(Fragment fragment, int contentView) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(contentView, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    public boolean addFragment(Fragment fragment, int contentView) {
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(contentView, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    public boolean addFragmentBackStack(Fragment fragment, int contentView) {
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(contentView, fragment)
                    .addToBackStack("")
                    .commit();
            return true;
        }
        return false;
    }

    public void backStackFragment() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public void onFragmentAttached() {

    }

    @Override
    public void onFragmentDetached(String tag) {

    }
}
