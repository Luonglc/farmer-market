package com.example.farmermarket.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.farmermarket.R;
import com.example.farmermarket.model.Item;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class SlideAdapter extends SliderViewAdapter<SlideAdapter.MViewHolder> {
    @Inject Context context;
    private List<Item> list ;

    @Inject
    public SlideAdapter(Context context) {
        this.context = context;
        list = new ArrayList<>();
    }

    public void renewItems(List<Item> sliderItems) {
        this.list = sliderItems;
        notifyDataSetChanged();
    }

    public void deleteItem(int position) {
        this.list.remove(position);
        notifyDataSetChanged();
    }

    public void addItem(Item sliderItem) {
        this.list.add(sliderItem);
        notifyDataSetChanged();
    }

    @Override
    public MViewHolder onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_slide, null);
        return new MViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(MViewHolder viewHolder, int position) {
        Item item = list.get(position);
        Glide.with(context)
                .load(item.getImageUrl())
                .fitCenter()
                .into(viewHolder.ivThumb);
        viewHolder.tvName.setText(item.getName());
        viewHolder.tvAddr.setText(item.getAddr());
        viewHolder.tvPrice.setText(item.getPrice());
    }

    @Override
    public int getCount() {
        return list.size();
    }

    public class MViewHolder extends SliderViewAdapter.ViewHolder {
        private ImageView ivThumb;
        private TextView tvName;
        private TextView tvPrice;
        private TextView tvAddr;
        public MViewHolder(View itemView) {
            super(itemView);
            ivThumb = itemView.findViewById(R.id.ivThumb);
            tvName = itemView.findViewById(R.id.tvName);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvAddr = itemView.findViewById(R.id.tvAddress);
        }
    }
}
